var app = angular.module('myApp', []).constant('API_URL','http://localhost:3000/api/');		
		app.controller('UserController', function($scope,$http,API_URL) {	  
		function getUsers(){
			$http.get(API_URL+"persona")
			.success(function(res){
				$scope.users=res;
			});
		}

		getUsers();

		$scope.insert= function (name) {
			$http.post(API_URL+"persona",{'name':name})
			.success(function(res){
				getUsers();
			});
		}

		$scope.update= function (i,name) {
			$http.put(API_URL+"persona/"+i,{'name':name})
			.success(function(res){
				getUsers();
			});
		}

		$scope.delete= function (i) {
			$http.delete(API_URL+"persona/"+i)
			.success(function(res){
				getUsers();
			});
		}
	});